package apps.swgame;

public class OneCardModel {
    int pathResource;
    int powerOfCard;
    boolean cardUsed;


    public OneCardModel(int path, int power) {
        this.pathResource = path;
        this.powerOfCard = power;
        cardUsed = false;
    }
}
