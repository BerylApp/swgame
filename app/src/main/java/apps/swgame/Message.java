package apps.swgame;
public class Message {
    public static final String SEND_CONFIG_LIST = "SEND_CONFIG_LIST";

    public String key;
    public Object data;

    public Message(String key, Object data){
        this.key = key;
        this.data = data;
    }



}
